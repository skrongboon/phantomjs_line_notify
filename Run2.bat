@echo off
title get sentry
SET /A reset=6
SET /A count=0
:loop
    RENAME dtt_ch5.png dtt_ch5_%count%.png
    ECHO "->Capture image : start"
    C:\xampp\htdocs\phantomJS_line-notify\phantomjs.exe index.js
    ECHO "->Capture image : complete"
    ECHO "->Send notify : start"
    C:\xampp\php\php -f sendNotify.php
    ECHO "->Send notify : complete"
    timeout /t 10000 /nobreak
    SET /A count =%count%+1
goto loop