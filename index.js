"use strict";
function waitFor(testFx, onReady, timeOutMillis) {
    var maxtimeOutMillis = timeOutMillis ? timeOutMillis : 3000, //< Default Max Timout is 3s
	start = new Date().getTime(),
	condition = false,
	interval = setInterval(function() {
		console.log("checked")
		if ( (new Date().getTime() - start < maxtimeOutMillis) && !condition ) {
			// If not time-out yet and condition not yet fulfilled
			condition = (typeof(testFx) === "string" ? eval(testFx) : testFx()); //< defensive code
		} else {
			if(!condition) {
				// If condition still not fulfilled (timeout but condition is 'false')
				console.log("'waitFor()' timeout");
				phantom.exit(1);
			} else {
				// Condition fulfilled (timeout and/or condition is 'true')
				console.log("'waitFor()' finished in " + (new Date().getTime() - start) + "ms.");
				typeof(onReady) === "string" ? eval(onReady) : onReady(); //< Do what it's supposed to do once the condition is fulfilled
				clearInterval(interval); //< Stop this interval
			}
		}
	}, 2000); //< repeat check every 250ms
};
var sys = require("system");
var  logResources = true;
var page = require('webpage').create();
page.viewportSize = { width: 1620, height: 950 };
var requestsArray = [];

page.onResourceRequested = function(requestData, networkRequest) {
  requestsArray.push(requestData.id);
};

page.onResourceReceived = function(response) {
  //console.log(response.)
  var index = requestsArray.indexOf(response.id);
  requestsArray.splice(index, 1);
};
//var urlToCapture = "http://172.17.82.15:9876/dtt_to_ch5/";
var urlToCapture = "http://172.17.82.46/rvw/";
var output = "./dtt_ch5.png";


/* page.open(urlToCapture, function(status) {
	 console.log("open: "+urlToCapture);
	if(status !=='success'){
		console.log("Load webpage error!");
		phantom.exit(1);
	}else{
		console.log("Load webpage complete");
		window.setTimeout(function(){
			page.render(output);
			console.log("render complete");
			phantom.exit();
			
		},200);
	} 
	//phantom.exit();
}); */

page.open(urlToCapture, function(status) {
	if(status == "success"){
		var c = 0;
		var interval = setInterval(function () {
		c++;
		//if (requestsArray.length === 0) {

			//clearInterval(interval);
			console.log("status:"+status);
			/* page.evaluate(function(){
				setTimeout(function(){
					console.log("Evaluate.");
					var enc = document.getElementById("enc_av_t4u_m");
					console.log(enc);
				},250)
				
			}); */
		  //console.log(content);
			//window.setTimeout(function(){
				page.render("dtt_ch5-"+c+".png");
				console.log("render complete");
				//phantom.exit();
				
			//},200);
		//}
		}, 4000);
	}else{
        console.log(status);
        phantom.exit();
    }
	
});
